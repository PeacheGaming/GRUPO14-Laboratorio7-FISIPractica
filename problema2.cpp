#include<iostream>
#include<windows.h>
#include <stdio.h>
using namespace std;
void limpiarBuffer();

struct nodo{
	int dato;
	nodo *sgte,*ant;
};
struct nodoSimple{
	int dato;
	nodoSimple *sgte;
};
typedef nodo *Tlista;
typedef nodoSimple *TlistaCircular;
typedef nodoSimple *TlistaSimple;

TlistaCircular lc=NULL;
TlistaSimple ls=NULL;
Tlista crearNodo(int valor){
	Tlista aux=new(nodo);
	aux->dato=valor;
	aux->sgte=NULL;
	aux->ant=NULL;
	return aux;
}
TlistaSimple crearNodoS(int valor){
	TlistaSimple aux=new(nodoSimple);
	aux->dato=valor;
	aux->sgte=NULL;
	return aux;
}
TlistaCircular crearNodoC(int valor){
	TlistaCircular aux=new(nodoSimple);
	aux->dato=valor;
	aux->sgte=aux;
	return aux;
}
void insertarNodo(Tlista &l1,int valor){
	Tlista aux=crearNodo(valor);
	if(l1==NULL){
		l1=aux;
	}else{
		aux->sgte=l1;
		l1->ant=aux;
		l1=aux;
	}
}
void insertarNodoS(TlistaSimple &l1,int valor){
	TlistaSimple aux=crearNodoS(valor);
	if(l1==NULL){
		l1=aux;
	}else{
		aux->sgte=l1;
		l1=aux;
	}
}
void insertarNodoC(TlistaCircular &l1,int valor){
	TlistaCircular temp=l1,aux=crearNodoC(valor);
	if(l1==NULL){
		l1=aux;
	}else{
		aux->sgte=l1;
		while(temp->sgte!=l1){
			temp=temp->sgte;
		}
		temp->sgte=aux;
		l1=aux;
	}
}
int media(Tlista l1){
	int suma=0;
	int i;
	for(i=0;l1!=NULL;i++){
		suma+=l1->dato;
	    l1=l1->sgte;	
	}
	return suma/i;
}
void Separar(Tlista l1){
	int m;
    if(l1==NULL){
      cout<<"\t\t\tLA LISTA ESTA VACIA";
	  system("pause>nul");   
    }else{
       m=media(l1);
       while(l1!=NULL){
       	 	if(l1->dato<=m){
       	 	insertarNodoC(lc,l1->dato);
       	 	cout<<l1->dato<<" "; system("pause>nul");     		
			}else{
       	 	insertarNodoS(ls,l1->dato);
			cout<<l1->dato<<" "; system("pause>nul");        	 	
			}
       	 	l1=l1->sgte;
       }
    }
    
}
void mostrar(Tlista l1){
     cout<<"\t\t\t";
     while(l1!=NULL){
        cout<<l1->dato;
        if(l1->sgte!=NULL)
            cout<<"->";
        l1=l1->sgte;
     }
     system("pause>nul");
}
void mostrar1(TlistaCircular l1){
	 TlistaCircular	temp=l1; 
	 cout<<"\t\t\t";
     while(l1->sgte!=temp){
        cout<<l1->dato;
        if(l1->sgte->sgte!=temp)
            cout<<"->";
        l1=l1->sgte;
     }
     cout<<"->"<<l1->dato;
     system("pause>nul");
}
void mostrar2(TlistaSimple l1){ 
	 cout<<"\t\t\t";
     while(l1!=NULL){
        cout<<l1->dato;
        if(l1->sgte!=NULL)
            cout<<"->";
        l1=l1->sgte;
     }
     system("pause>nul");
}

void menu(Tlista &l1){
	int p;
	int mediana=0;
	if(l1!=NULL)
	mediana=media(l1);
	system("cls");
	cout << "\t\t\t浜様様様様様様様様様様様様様様様様様融" << endl;
	cout << "\t\t\t�                MENU                �" << endl;
	cout << "\t\t\t麺様様様様様様様様様様様様様様様様様郵" << endl;
	cout << "\t\t\t�  1. Insertar inicio                �" << endl;
	cout << "\t\t\t�  2. Separar                        �" << endl;
	cout << "\t\t\t�  3. Mostrar lista simple circular  �" << endl;
	cout << "\t\t\t�  4. Mostrar lista simple           �" << endl;
	cout << "\t\t\t�  5. Mostrar Lista Original         �" << endl;
	cout << "\t\t\t�  6. Salir                          �" << endl;
	cout << "\t\t\t藩様様様様様様様様様様様様様様様様様夕" << endl;
	cout << "\t\t\t\tIngrese una opcion: ";	cin>>p;
	switch (p){
	case 1: int valor;
	        cout<<"\t\t\tIngresar VALOR : ";  cin>>valor;
	        if(!cin.fail()){
	        insertarNodo(l1,valor);
			cout<<"\t\t\tOPERACION EXITOSA!";
	        system("pause>nul"); 
			}else{
			cout<<"\t\t\tVALOR INVALIDO";
		    limpiarBuffer();
			}
			menu(l1); 
			break;
	case 2: if(l1!=NULL){
	        Separar(l1); 
	        cout<<"\t\t\tOPERACION EXITOSA!";
	        system("pause>nul");
			}else{
		    cout<<"\t\t\tLISTA VACIA";
		    system("pause>nul");
			}
			menu(l1); break;
	case 3: if(lc!=NULL)
	        mostrar1(lc); 
			else{
			cout<<"\t\t\tLISTA CIRCULAR VACIA";
			system("pause>nul");
			}
			menu(l1); break;
	case 4:	if(ls!=NULL)
	        mostrar2(ls); 
			else{
			cout<<"\t\t\tLISTA SIMPLE VACIA";
			system("pause>nul");	
			}
			menu(l1); break;
    case 5:	if(l1!=NULL){
	        mostrar(l1);
	        cout<<endl;
			cout<<"\t\t\tMEDIA : "<<mediana; 
			system("pause>nul");
			}else{
			cout<<"\t\t\tLISTA TOTAL VACIA";
			system("pause>nul");	
			}
			menu(l1); break;
	case 6: system("exit"); break;
	default: cout << "Opcion invalida!\n";
		limpiarBuffer();
		menu(l1); break;
	}
}
void limpiarBuffer(){
	system("pause > nul");
	std::cin.clear();
	std::cin.ignore(1000, '\n');
}
int main(){
	Tlista l1=NULL;
	menu(l1);
}
