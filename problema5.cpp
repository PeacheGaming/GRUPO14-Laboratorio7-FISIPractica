#include<iostream>
#include<windows.h>
#include <stdio.h>
using namespace std;
void limpiarBuffer();

struct nodo{
	char letra;
	nodo *sgte,*ant;
};

typedef nodo *Tnodo;

Tnodo crearNodo(char valor){
	Tnodo aux=new(nodo);
	aux->letra=valor;
	aux->sgte=NULL;
	aux->ant=NULL;
	return aux;
}
void insertarInicio(Tnodo &l1,char valor){
	Tnodo nuevo =crearNodo(valor);
	if(l1==NULL){
		l1=nuevo;
	}else{
		nuevo->sgte=l1;
		l1->ant=nuevo;
		l1=nuevo;
	}
}

void insertarFinal(Tnodo &l1,char valor){
	if(l1==NULL){
		insertarInicio(l1,valor);
	}
	else{
		Tnodo nuevo =crearNodo(valor);
		Tnodo aux = l1;
		while(aux->sgte!=NULL){
			aux=aux->sgte;
		}
		aux->sgte=nuevo;
		nuevo->ant=aux;
	}
}

void insertarPosicion(Tnodo &l1,char valor,int n){
	if(n==1){
		insertarInicio(l1,valor);
	}
	else{
		Tnodo nuevo = crearNodo(valor);
		int i=1;
		Tnodo aux = l1;
		while(i<n && aux!=NULL){
			aux=aux->sgte;
			i++;
		}
		if(aux==NULL){
			cout<<"Posicion no encontrada en dicha lista"<<endl;
		}
		else{
			nuevo->sgte=aux;
			nuevo->ant=aux->ant;
			aux->ant->sgte=nuevo;
			aux->ant=nuevo;
		}
	}
}

void mostrar(Tnodo l1){
     Tnodo aux=l1;
     while(aux->sgte!=NULL){
     	cout<<" -> "<<aux->letra;
     	aux=aux->sgte;
	 }
	 cout<<" -> "<<aux->letra<<endl<<endl;
}

void mostrarInvertido(Tnodo l1){
	Tnodo aux=l1;
	while(aux->sgte!=NULL){
		aux=aux->sgte;
	}
	while(aux->ant!=NULL){
		cout<<" <- "<<aux->letra;
		aux=aux->ant;
	}
	cout<<" <- "<<aux->letra<<endl;
}

int main(){
	Tnodo l1=NULL;
	insertarInicio(l1,'N');
	mostrar(l1); 
	insertarFinal(l1,'M');
	mostrar(l1);
	insertarFinal(l1,'L');
	mostrar(l1);
	insertarInicio(l1,'A');
	mostrar(l1);
	insertarPosicion(l1,'I',3);
	mostrar(l1);
	insertarPosicion(l1,'A',5);
	mostrar(l1);
	mostrarInvertido(l1);
	
}
